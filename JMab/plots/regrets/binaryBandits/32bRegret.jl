#Plots algorithms for 8 normal bandits.
using JMab
using DataFrames
using Gadfly
using JLD

const nArms = 32
const plays = 1000
const sample = 1
const asamp = 1000

b32Single = load("./plots/regrets/binaryBandits/singleAlgo/b32Single.jld")

b32Tree = load("./plots/regrets/binaryBandits/treesNet/b32Trees.jld")
b32Flat = load("./plots/regrets/binaryBandits/flatNet/b32Flat.jld")

rppdfBayes = JMab.addTotalCuRegret(b32Single["rppdfBayes"])
rppdfBayes2 = JMab.addTotalCuRegret(b32Single["rppdfBayes2"])

rppdfBiSortTSTree = JMab.addTotalCuRegret(b32Tree["rppdfBiSortTSTree"])
rppdfOLTree = JMab.addTotalCuRegret(b32Tree["rppdfOLTree"])
rppdfBiTree = JMab.addTotalCuRegret(b32Tree["rppdfBiTree"])

rppdfCliqueNet = JMab.addTotalCuRegret(b32Flat["rppdfCliqueNet"])
rppdfLocGlobNetRelW = JMab.addTotalCuRegret(b32Flat["rppdfLocGlobNetRelW"])
rppdfLocGlobNetTS = JMab.addTotalCuRegret(b32Flat["rppdfLocGlobNetTS"])

#Combine and plot all algorithms
rppdf = vcat(
  #rppdfBayes,
  rppdfBayes2,
  #rppdfOLTree,
  #rppdfBiTree,
  rppdfBiSortTSTree,
  #rppdfCliqueNet,
  #rppdfLocGlobNetRelW,
  #rppdfLocGlobNetTS,
  )

white_panel = Theme(
  #panel_fill="white",
  background_color="white",
  panel_opacity=1,
  grid_color="black",
  minor_label_color="black",
  #minor_label_font_size=15pt,
  #point_label_font_size=15pt,
  #line_width=2pt
  #major_label_font_size=20pt,
  #key_label_font_size=10pt,
  )

p = plot(rppdf, x=:x, y=:y, color=:label,
  Geom.line,
  #Geom.point,
  #layer(Geom.smooth),
  Guide.YLabel("Regret %"),
  Guide.XLabel("Plays"),
  Guide.Title("Regret % per Play over $nArms Bernoulli Bandits"),
  white_panel
  )
#

#draw(PNG("plots/print/b32AllRegrets.png", 10inch, 6inch), p)
