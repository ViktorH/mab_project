#Plots algorithms for 8 normal bandits.
using JMab
using DataFrames
using Gadfly
using JLD

const nArms = 8
const plays = 10000
const sample = 1
const asamp = 1

b8Single = load("./plots/regrets/binaryBandits/singleAlgo/b8Single.jld")
b8Tree = load("./plots/regrets/binaryBandits/treesNet/b8Trees.jld")
b8Flat = load("./plots/regrets/binaryBandits/flatNet/b8Flat.jld")

rppdfBayes = JMab.addTotalCuRegret(b8Single["rppdfBayes"])
rppdfBayes2 = JMab.addTotalCuRegret(b8Single["rppdfBayes2"])
rppdfEgreedy = JMab.addTotalCuRegret(b8Single["rppdfEgreedy"])
rppdfUCB = JMab.addTotalCuRegret(b8Single["rppdfUCB"])

rppdfBiSortTSTree = JMab.addTotalCuRegret(b8Tree["rppdfBiSortTSTree"])
rppdfOLTree = JMab.addTotalCuRegret(b8Tree["rppdfOLTree"])
rppdfBiTree = JMab.addTotalCuRegret(b8Tree["rppdfBiTree"])
rppdfColTree = JMab.addTotalCuRegret(b8Tree["rppdfColTree"])

rppdfCliqueNet = JMab.addTotalCuRegret(b8Flat["rppdfCliqueNet"])
rppdfLocGlobNetRelW = JMab.addTotalCuRegret(b8Flat["rppdfLocGlobNetRelW"])
rppdfLocGlobNetTS = JMab.addTotalCuRegret(b8Flat["rppdfLocGlobNetTS"])

#Combine and plot all algorithms
rppdf = vcat(
  rppdfBayes,
  rppdfUCB,
  rppdfEgreedy,
  #rppdfBayes2,
  #rppdfOLTree,
  #rppdfBiTree,
  #rppdfBiSortTSTree,
  #rppdfCliqueNet,
  #rppdfLocGlobNetRelW,
  #rppdfLocGlobNetTS,
  )

  white_panel = Theme(
    #panel_fill="white",
    background_color="white",
    panel_opacity=1,
    grid_color="black",
    minor_label_color="black",
    #minor_label_font_size=15pt,
    #point_label_font_size=15pt,
    #line_width=2pt
    #major_label_font_size=20pt,
    #key_label_font_size=10pt,
    )

p = plot(rppdf, x=:x, y=:y, color=:label,
  Geom.line,
  #Geom.point,
  #layer(Geom.smooth),
  Guide.YLabel("Regret %"),
  Guide.XLabel("Plays"),
  Guide.Title("Regret % per Play over $nArms Bernoulli Bandits"),
  white_panel
  )
#

#draw(PNG("plots/print/b8AllRegrets.png", 10inch, 6inch), p)
