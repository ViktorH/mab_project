#Plots algorithms for 8 normal bandits.
using JMab
using DataFrames
using Gadfly
using JLD

const nArms = 128
const plays = 4000
const sample = 1
const asamp = 1000

b128Single = load("./plots/regrets/binaryBandits/singleAlgo/b128Single2k.jld")
b128Tree = load("./plots/regrets/binaryBandits/treesNet/b128Trees2k.jld")
b128Flat = load("./plots/regrets/binaryBandits/flatNet/b128Flat4k.jld")

rppdfBayes = JMab.addTotalCuRegret(b128Single["rppdfBayes"])
rppdfBayes2 = JMab.addTotalCuRegret(b128Single["rppdfBayes2"])

rppdfBiSortTSTree = JMab.addTotalCuRegret(b128Tree["rppdfBiSortTSTree"])
rppdfOLTree = JMab.addTotalCuRegret(b128Tree["rppdfOLTree"])
rppdfBiTree = JMab.addTotalCuRegret(b128Tree["rppdfBiTree"])
rppdfColTree = JMab.addTotalCuRegret(b128Tree["rppdfColTree"])

rppdfCliqueNet = JMab.addTotalCuRegret(b128Flat["rppdfCliqueNet"])
rppdfLocGlobNetRelW = JMab.addTotalCuRegret(b128Flat["rppdfLocGlobNetRelW"][1:2000,:])
rppdfLocGlobNetTS = JMab.addTotalCuRegret(b128Flat["rppdfLocGlobNetTS"][1:2000,:])

#Combine and plot all algorithms
rppdf = vcat(
  #rppdfBayes,
  rppdfBayes2,
  #rppdfOLTree,
  #rppdfBiTree,
  rppdfBiSortTSTree,
  #rppdfColTree,
  #rppdfCliqueNet,
  rppdfLocGlobNetRelW,
  #rppdfLocGlobNetTS,
  )

white_panel = Theme(
  #panel_fill="white",
  background_color="white",
  panel_opacity=1,
  grid_color="black",
  minor_label_color="black",
  #minor_label_font_size=15pt,
  #point_label_font_size=15pt,
  #line_width=2pt
  #major_label_font_size=20pt,
  #key_label_font_size=10pt,
  )

p = plot(rppdf, x=:x, y=:y, color=:label,
  Geom.line,
  #Geom.point,
  #layer(Geom.smooth),
  Guide.YLabel("Regret %"),
  Guide.XLabel("Plays"),
  Guide.Title("Regret % per Play over $nArms Bernoulli Bandits"),
  white_panel
  )
#

#draw(PNG("plots/print/b128AllRegrets.png", 10inch, 6inch), p)
#draw(PNG("plots/print/b128LocGlobRelWVsTS4k.png", 10inch, 6inch), p)
