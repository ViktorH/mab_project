#Plots algorithms for 8 normal bandits.
using JMab
using DataFrames
using Gadfly
using JLD

const nArms = 128
const plays = 2000
const sample = 1
const asamp = 1

# Tree networks
algoBi421 = JMab.defaultBiTree(ones(nArms),JMab.tsampling)
resBi421 = JMab.biTreeBuilder(algoBi421,ones(nArms))
# BiTSSortSplit
rppBiSortTSTree = JMab.basicSampler(asamp,nArms,JMab.binerySplitNest, resBi421, plays, sample,
  JMab.trialBiSort,algoBi421)
rppdfBiSortTSTree = DataFrame(x = 1:plays, y = rppBiSortTSTree, label = "BiSortTSTree")

#Combine and plot all algorithms
rppdf = vcat(
  rppdfCliqueNet,
  rppdfLocGlobNetRelW,
  rppdfLocGlobNetTS,
  )

p = plot(rppdf, x=:x, y=:y, color=:label,
  Geom.line,
  #Geom.point,
  #layer(Geom.smooth),
  Guide.YLabel("Regret %"),
  Guide.XLabel("Plays"),
  Guide.Title("Regret % per Play over $nArms Bernoulli Bandits"),
  #Theme(panel_fill="white")
  )
#

ws = Dict([
  ("rppdfCliqueNet",rppdfCliqueNet),
  ("rppdfLocGlobNetRelW",rppdfLocGlobNetRelW),
  ("rppdfLocGlobNetTS",rppdfLocGlobNetTS),
  ])
#

#rppdfs = load("./plots/regrets/binaryBandits/flatNet/b128flat.jld")
#save("./plots/regrets/binaryBandits/flatNet/bFlat.jld", ws)
#draw(PNG("plots/print/128bFlatNets.png", 10inch, 6inch), p)
