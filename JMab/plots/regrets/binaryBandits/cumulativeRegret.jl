#Plots Cumulative Regret.
using JMab
using DataFrames
using Gadfly
using JLD

const nArms = 32
const plays = 2000
const sample = 1
const asamp = 1000

b32Single = load("./plots/regrets/binaryBandits/singleAlgo/b32Single.jld")
b32Tree = load("./plots/regrets/binaryBandits/treesNet/b32Trees.jld")
b32Flat = load("./plots/regrets/binaryBandits/flatNet/b32Flat.jld")

rppdfBayes = b32Single["rppdfBayes"]
rppdfBayes2 = b32Single["rppdfBayes2"]

rppdfBiSortTSTree = b32Tree["rppdfBiSortTSTree"]
rppdfOLTree = b32Tree["rppdfOLTree"]
rppdfBiTree = b32Tree["rppdfBiTree"]
rppdfColTree = b32Tree["rppdfColTree"]

rppdfCliqueNet = b32Flat["rppdfCliqueNet"]
rppdfLocGlobNetRelW = b32Flat["rppdfLocGlobNetRelW"]
rppdfLocGlobNetTS = b32Flat["rppdfLocGlobNetTS"]
########
rppdfBayes[2] = cumsum(rppdfBayes[2])
rppdfBayes2[2] = cumsum(rppdfBayes2[2])
rppdfBiSortTSTree[2] = cumsum(rppdfBiSortTSTree[2])
rppdfColTree[2] = cumsum(rppdfColTree[2])
rppdfLocGlobNetRelW[2] = cumsum(rppdfLocGlobNetRelW[2])
rppdfLocGlobNetTS[2] = cumsum(rppdfLocGlobNetTS[2])
########


#Combine and plot all algorithms
rppdf = vcat(
  #rppdfBayes,
  rppdfBayes2,
  rppdfLocGlobNetRelW,
  rppdfBiSortTSTree,
  #rppdfColTree,
  #rppdfBiTree,
  #rppdfOLTree
  )
#

#http://gadflyjl.org/stable/man/themes.html
white_panel = Theme(
  #panel_fill="white",
  background_color="white",
  panel_opacity=1,
  grid_color="black",
  minor_label_color="black",
  #minor_label_font_size=15pt,
  #point_label_font_size=15pt,
  #line_width=2pt
  #major_label_font_size=20pt,
  #key_label_font_size=10pt,
  )

p = plot(rppdf, x=:x, y=:y, color=:label,
  Geom.line,
  #Geom.point,
  #layer(Geom.smooth),
  Guide.YLabel("Cumulative Regret"),
  Guide.XLabel("Plays"),
  Guide.Title("Cumulative Regret per Play over $nArms Bernoulli Bandits"),
  white_panel,
  )
#

#draw(PNG("plots/print/b32AllRegrets.png", 10inch, 6inch), p)
