#Plots algorithms for 8 normal bandits.
using JMab
using DataFrames
using Gadfly
using JLD

const nArms = 128
const plays = 4000
const sample = 1
const asamp = 1000

#Networked algorithms
const algoClick = [JMab.tsampling for i in 1:nArms]
const resClick = [ones(Int,2,nArms) for i in 1:nArms]

# LocGlobNetRelW
rppLocGlobNet = JMab.basicSampler(asamp,nArms,JMab.locGlobNetRelW, resClick, plays, sample,
  JMab.trialCliqueNet,algoClick)
rppdfLocGlobNetRelW = DataFrame(x = 1:plays, y = rppLocGlobNet, label = "LocGlobNetRelW")

# LocGlobNetTS
rppLocGlobNetTS = JMab.basicSampler(asamp,nArms,JMab.locGlobNetTS, resClick, plays, sample,
  JMab.trialCliqueNet,algoClick)
rppdfLocGlobNetTS = DataFrame(x = 1:plays, y = rppLocGlobNetTS, label = "LocGlobNetTS")

# CliqueNet
rppCliqueNet = JMab.basicSampler(asamp,nArms,JMab.cliqueNet, resClick, plays, sample,
  JMab.trialCliqueNet,algoClick)
rppdfCliqueNet = DataFrame(x = 1:plays, y = rppCliqueNet, label = "CliqueNet")


#Combine and plot all algorithms
rppdf = vcat(
  rppdfCliqueNet,
  rppdfLocGlobNetRelW,
  rppdfLocGlobNetTS,
  )

p = plot(rppdf, x=:x, y=:y, color=:label,
  Geom.line,
  #Geom.point,
  #layer(Geom.smooth),
  Guide.YLabel("Regret %"),
  Guide.XLabel("Plays"),
  Guide.Title("Regret % per Play over $nArms Bernoulli Bandits"),
  #Theme(panel_fill="white")
  )
#

wss = Dict([
  #("rppdfCliqueNet",rppdfCliqueNet),
  ("rppdfLocGlobNetRelW",rppdfLocGlobNetRelW),
  ("rppdfLocGlobNetTS",rppdfLocGlobNetTS),
  ])
#

#rppdfs = load("./plots/regrets/binaryBandits/flatNet/b128flat.jld")
#save("./plots/regrets/binaryBandits/flatNet/b128Flat4k.jld", wss)
#draw(PNG("plots/print/128bFlatNets.png", 10inch, 6inch), p)
