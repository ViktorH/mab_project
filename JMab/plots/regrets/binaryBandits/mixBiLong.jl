#Plots algorithms for 8 normal bandits.
using JMab
using DataFrames
using Gadfly
using JLD

const nArms = 32
const plays = 2000
const sample = 1
const asamp = 500

#Networked algorithms
const algoClick = [JMab.tsampling for i in 1:nArms]
const resClick = [ones(Int,2,nArms) for i in 1:nArms]

# Bayesian Bandit
rppBayes = JMab.basicSampler(asamp,nArms,JMab.bayesB, ones(Int,2,nArms), plays, sample,
  JMab.trialSimpleAlgo)
rppdfBayes = DataFrame(x = 1:plays, y = rppBayes, label = "Bayes")

# Squared Basyesin Bandit
rppBayes2 = JMab.basicSampler(asamp,nArms,JMab.bayesB2, ones(Int,2,nArms), plays, sample,
  JMab.trialSimpleAlgo)
rppdfBayes2 = DataFrame(x = 1:plays, y = rppBayes2, label = "Bayes2")

# Tree networks
algoBi421 = JMab.defaultBiTree(ones(nArms),JMab.tsampling)
resBi421 = JMab.biTreeBuilder(algoBi421,ones(nArms))
# BiTSSortSplit
rppBiSortTSTree = JMab.basicSampler(asamp,nArms,JMab.binerySplitNest, resBi421, plays, sample,
  JMab.trialBiSort,algoBi421)
rppdfBiSortTSTree = DataFrame(x = 1:plays, y = rppBiSortTSTree, label = "BiSortTSTree")

# Collective Tree
algoCol = JMab.defaultBiTree(ones(nArms),JMab.tsampling)
resCol = JMab.treeBuilder(algoCol,ones(nArms))
rppColTree = JMab.basicSampler(asamp,nArms,JMab.lgNester, resCol, plays, sample,
  JMab.trialSieve,algoCol)
rppdfColTree = DataFrame(x = 1:plays, y = rppColTree, label = "ColTree")

# LocGlobNetRelW
rppLocGlobNet = JMab.basicSampler(asamp,nArms,JMab.locGlobNetRelW, resClick, plays, sample,
  JMab.trialCliqueNet,algoClick)
rppdfLocGlobNetRelW = DataFrame(x = 1:plays, y = rppLocGlobNet, label = "LocGlobNetRelW")

# LocGlobNetTS
rppLocGlobNetTS = JMab.basicSampler(asamp,nArms,JMab.locGlobNetTS, resClick, plays, sample,
  JMab.trialCliqueNet,algoClick)
rppdfLocGlobNetTS = DataFrame(x = 1:plays, y = rppLocGlobNetTS, label = "LocGlobNetTS")

#Combine and plot all algorithms
rppdf = vcat(
  #rppdfBayes,
  rppdfBayes2,
  #rppdfOLTree,
  #rppdfBiTree,
  rppdfBiSortTSTree,
  #rppdfCliqueNet,
  rppdfLocGlobNetRelW,
  #rppdfLocGlobNetTS,
  )

p = plot(rppdf, x=:x, y=:y, color=:label,
  Geom.line,
  #Geom.point,
  #layer(Geom.smooth),
  Guide.YLabel("Regret %"),
  Guide.XLabel("Plays"),
  Guide.Title("Regret % per Play over $nArms Bernoulli Bandits"),
  #Theme(panel_fill="white")
  )
#

#draw(PNG("plots/print/b128BitSortTSVsBayes2Long4k.png", 10inch, 6inch), p)

sum(rppBayes2)/1
sum(rppBiSortTSTree)/1
sum(rppLocGlobNet)/1
rppBayes2[1] + rppBayes2[2] + rppBayes2[3]


rppBayes2[1]
rppBiSortTSTree[1]
rppLocGlobNet[1]
