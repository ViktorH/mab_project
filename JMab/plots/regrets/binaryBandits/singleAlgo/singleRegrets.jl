#Plots algorithms for 8 normal bandits.
using JMab
using DataFrames
using Gadfly
using JLD

const nArms = 8
const plays = 100000
const sample = 1
const asamp = 10

# Bayesian Bandit
rppBayes = JMab.basicSampler(asamp,nArms,JMab.bayesB, ones(Int,2,nArms), plays, sample,
  JMab.trialSimpleAlgo)
rppdfBayes = DataFrame(x = 1:plays, y = rppBayes, label = "Bayes")

# Squared Basyesin Bandit
rppBayes2 = JMab.basicSampler(asamp,nArms,JMab.bayesB2, ones(Int,2,nArms), plays, sample,
  JMab.trialSimpleAlgo)
rppdfBayes2 = DataFrame(x = 1:plays, y = rppBayes2, label = "Bayes2")

# UCB1
rppUCB = JMab.basicSampler(asamp,nArms,JMab.ucb1, ones(Int,2,nArms), plays, sample,
  JMab.trialSimpleAlgo)
rppdfUCB = DataFrame(x = 1:plays, y = rppUCB, label = "UCB")

# Egreedy
rppEgreedy = JMab.basicSampler(asamp,nArms,JMab.eGreedy, ones(Int,2,nArms), plays, sample,
  JMab.trialSimpleAlgo)

rppdfEgreedy = DataFrame(x = 1:plays, y = rppEgreedy, label = "eGreedy")

#Combine and plot all algorithms
rppdf = vcat(
  rppdfBayes,
  #rppdfBayes2,
  #rppdfUCB,
  #rppdfEgreedy
  )

white_panel = Theme(
  #panel_fill="white",
  background_color="white",
  panel_opacity=1,
  grid_color="black",
  minor_label_color="black",
  #minor_label_font_size=15pt,
  #point_label_font_size=15pt,
  #line_width=2pt
  #major_label_font_size=20pt,
  #key_label_font_size=10pt,
  )

p = plot(rppdf, x=:x, y=:y, color=:label,
  Geom.line,
  #Geom.point,
  #layer(Geom.smooth),
  Guide.YLabel("Regret"),
  Guide.XLabel("Plays"),
  Guide.Title("Regret per Play over $nArms Bernoulli Bandits"),
  white_panel
  )
#
ws = Dict([
  ("rppdfBayes",rppdfBayes),
  #("rppdfBayes2",rppdfBayes2),
  #("rppdfUCB",rppdfUCB),
  #("rppdfEgreedy",rppdfEgreedy)
  ])
#

#save("./plots/regrets/binaryBandits/singleAlgo/b128Single4k.jld", ws)
#draw(PNG("plots/print/b128Long2kSqBayes.png", 10inch, 6inch), p)
