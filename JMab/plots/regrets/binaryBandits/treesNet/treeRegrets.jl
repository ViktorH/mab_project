#Plots algorithms for 8 normal bandits.
using JMab
using DataFrames
using Gadfly
using JLD

const nArms = 128
const plays = 2000
const sample = 1
const asamp = 1000

# Tree networks
algoBi421 = JMab.defaultBiTree(ones(nArms),JMab.tsampling)
resBi421 = JMab.biTreeBuilder(algoBi421,ones(nArms))
# BiTSSortSplit
rppBiSortTSTree = JMab.basicSampler(asamp,nArms,JMab.binerySplitNest, resBi421, plays, sample,
  JMab.trialBiSort,algoBi421)
rppdfBiSortTSTree = DataFrame(x = 1:plays, y = rppBiSortTSTree, label = "BiSortTSTree")

# Normal Bi Tree
rppBiTree = JMab.basicSampler(asamp,nArms,JMab.binerySplitNest, resBi421, plays, sample,
  JMab.trialBiNest,algoBi421)
rppdfBiTree = DataFrame(x = 1:plays, y = rppBiTree, label = "BiTree")

# Overlapping Tree
algoOL = JMab.defaultBiTree(ones(nArms),JMab.tsampling)
resOL = JMab.treeBuilder(algoOL,ones(nArms))
rppOLTree = JMab.basicSampler(asamp,nArms,JMab.nester, resOL, plays, sample,
  JMab.trialSieve,algoOL)
rppdfOLTree = DataFrame(x = 1:plays, y = rppOLTree, label = "OLTree")

# Collective Tree
algoCol = JMab.defaultBiTree(ones(nArms),JMab.tsampling)
resCol = JMab.treeBuilder(algoCol,ones(nArms))
rppColTree = JMab.basicSampler(asamp,nArms,JMab.lgNester, resCol, plays, sample,
  JMab.trialSieve,algoCol)
rppdfColTree = DataFrame(x = 1:plays, y = rppColTree, label = "ColTree")

#Combine and plot all algorithms
rppdf = vcat(
  rppdfColTree,
  #rppdfOLTree,
  #rppdfBiTree,
  rppdfBiSortTSTree,
  )

  #http://gadflyjl.org/stable/man/themes.html
  white_panel = Theme(
    #panel_fill="white",
    background_color="white",
    panel_opacity=1,
    grid_color="black",
    minor_label_color="black",
    #minor_label_font_size=15pt,
    #point_label_font_size=15pt,
    #line_width=2pt
    #major_label_font_size=20pt,
    #key_label_font_size=10pt,
    )

p = plot(rppdf, x=:x, y=:y, color=:label,
  Geom.line,
  #Geom.point,
  #layer(Geom.smooth),
  Guide.YLabel("Regret %"),
  Guide.XLabel("Plays"),
  Guide.Title("Regret % per Play over $nArms Bandits"),
  white_panel
  )
#

ws = Dict([
  #("rppdfOLTree",rppdfOLTree),
  #("rppdfBiTree",rppdfBiTree),
  ("rppdfBiSortTSTree",rppdfBiSortTSTree),
  ("rppdfColTree",rppdfColTree),
  ])
#
#save("./plots/regrets/binaryBandits/treesNet/b128Trees2k.jld", ws)
#rppdfs = load("./plots/regrets/binaryBandits/treesNet/b128TreesVsFlatNetVsSingle.jld")
#draw(PNG("plots/print/128bFlatNets.png", 10inch, 6inch), p)
