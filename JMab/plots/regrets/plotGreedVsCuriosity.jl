#greed vs Curiosity Regret
#regretPlot
using JMab

#include("setupRegret.jl")

function greedy(results::Matrix, arms::Array, explorePr = 0)
   return JMab.eGreedy(results,arms,explorePr)
end

function curious(results::Matrix, arms::Array, explorePr = 1)
   return JMab.eGreedy(results,arms,explorePr)
end

#rppGreedy = JMab.regretPerPlayREFAC(greedy,armsPr,plays,sample)
rppGreedy = JMab.regretPerPlayREFAC(greedy, ones(Int,2,length(armsPr)), armsPr, plays, sample,
  JMab.trialREFAC, JMab.setNbandits)
rppdfGreedy = DataFrame(x = 1:plays, y = rppGreedy, label = "Greedy")

#rppCurious = JMab.regretPerPlay(curious,armsPr,plays,sample)
rppCurious = JMab.regretPerPlayREFAC(curious, ones(Int,2,length(armsPr)), armsPr, plays, sample,
  JMab.trialREFAC, JMab.setNbandits)
rppdfCurious = DataFrame(x = 1:plays, y = rppCurious, label = "Curious")

#Bayes as a proof of optimal curiosity and greed
rppBayesB = JMab.regretPerPlayREFAC(JMab.bayesB, ones(Int,2,length(armsPr)), armsPr, plays, sample,
  JMab.trialREFAC, JMab.setNbandits)
rppdfBayes = DataFrame(x = 1:plays, y = rppBayesB, label = "Bayes")

#Combine and plot all algorithms
rppdf = vcat(
  rppdfGreedy,
  rppdfCurious,
  rppdfBayes,
  )

p = plot(rppdf, x=:x, y=:y, color=:label,
  Geom.line,
  #Geom.point,
  #layer(Geom.smooth),
  Guide.YLabel("Regret"),
  Guide.XLabel("Plays"),
  Guide.Title("Regret per Play"))
#
#draw(PNG("myplot.png", 903px, 567px), p)
