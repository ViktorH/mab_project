#Plots algorithms for 128 normal bandits.
using JMab
using DataFrames
using Gadfly
using JLD

const nArms = 128
const plays = 1000
const sample = 1
const asamp = 1000

symb128Single = load("./plots/regrets/symbioticBandits/singleAlgo/symb128Single.jld")
symb128Tree = load("./plots/regrets/symbioticBandits/treeNet/symb128Tree.jld")
symb128Flat = load("./plots/regrets/symbioticBandits/flatNet/symb128Flat.jld")

rppdfBayes = JMab.addTotalCuRegret(symb128Single["rppdfBayes"])
rppdfBayes2 = JMab.addTotalCuRegret(symb128Single["rppdfBayes2"])

rppdfBiSortTSTree = JMab.addTotalCuRegret(symb128Tree["rppdfBiSortTSTree"])
rppdfOLTree = JMab.addTotalCuRegret(symb128Tree["rppdfOLTree"])
rppdfBiTree = JMab.addTotalCuRegret(symb128Tree["rppdfBiTree"])

rppdfCliqueNet = JMab.addTotalCuRegret(symb128Flat["rppdfCliqueNet"])
rppdfLocGlobNetRelW = JMab.addTotalCuRegret(symb128Flat["rppdfLocGlobNetRelW"])
rppdfLocGlobNetTS = JMab.addTotalCuRegret(symb128Flat["rppdfLocGlobNetTS"])

#Combine and plot all algorithms
rppdf = vcat(
  rppdfBayes,
  #rppdfBayes2,
  rppdfOLTree,
  rppdfBiTree,
  rppdfBiSortTSTree,
  rppdfCliqueNet,
  rppdfLocGlobNetRelW,
  rppdfLocGlobNetTS,
  )

white_panel = Theme(
  #panel_fill="white",
  background_color="white",
  panel_opacity=1,
  grid_color="black",
  minor_label_color="black",
  #minor_label_font_size=15pt,
  #point_label_font_size=15pt,
  #line_width=2pt
  #major_label_font_size=20pt,
  #key_label_font_size=10pt,
  )


p = plot(rppdf, x=:x, y=:y, color=:label,
  Geom.line,
  #Geom.point,
  #layer(Geom.smooth),
  Guide.YLabel("Regret %"),
  Guide.XLabel("Plays"),
  Guide.Title("Regret % per Play over $nArms Symbiotic Bandits"),
  white_panel
  )
#

#draw(PNG("plots/print/symb128AllRegrets.png", 10inch, 6inch), p)
