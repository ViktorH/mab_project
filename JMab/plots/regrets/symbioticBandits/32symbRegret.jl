#Plots algorithms for 32 normal bandits.
using JMab
using DataFrames
using Gadfly
using JLD

const nArms = 32
const plays = 1000
const sample = 1
const asamp = 1000

symb32Single = load("./plots/regrets/symbioticBandits/singleAlgo/symb32Single2k.jld")
symb32Tree = load("./plots/regrets/symbioticBandits/treeNet/symb32Tree2k.jld")
symb32Flat = load("./plots/regrets/symbioticBandits/flatNet/symb32Flat2k.jld")

rppdfBayes = JMab.addTotalCuRegret(symb32Single["rppdfBayes"])
rppdfBayes2 = JMab.addTotalCuRegret(symb32Single["rppdfBayes2"])

rppdfBiSortTSTree = JMab.addTotalCuRegret(symb32Tree["rppdfBiSortTSTree"])
rppdfOLTree = JMab.addTotalCuRegret(symb32Tree["rppdfOLTree"])
rppdfBiTree = JMab.addTotalCuRegret(symb32Tree["rppdfBiTree"])
rppdfColTree = JMab.addTotalCuRegret(symb32Tree["rppdfColTree"])

rppdfCliqueNet = JMab.addTotalCuRegret(symb32Flat["rppdfCliqueNet"])
rppdfLocGlobNetRelW = JMab.addTotalCuRegret(symb32Flat["rppdfLocGlobNetRelW"])
rppdfLocGlobNetTS = JMab.addTotalCuRegret(symb32Flat["rppdfLocGlobNetTS"])

#Combine and plot all algorithms
rppdf = vcat(
  rppdfBayes,
  #rppdfBayes2,
  rppdfOLTree,
  #rppdfColTree,
  rppdfBiTree,
  rppdfBiSortTSTree,
  rppdfCliqueNet,
  rppdfLocGlobNetRelW,
  rppdfLocGlobNetTS,
  )

white_panel = Theme(
  #panel_fill="white",
  background_color="white",
  panel_opacity=1,
  grid_color="black",
  minor_label_color="black",
  #minor_label_font_size=15pt,
  #point_label_font_size=15pt,
  #line_width=2pt
  #major_label_font_size=20pt,
  #key_label_font_size=10pt,
  )

p = plot(rppdf, x=:x, y=:y, color=:label,
  Geom.line,
  #Geom.point,
  #layer(Geom.smooth),
  Guide.YLabel("Regret %"),
  Guide.XLabel("Plays"),
  Guide.Title("Regret % per Play over $nArms Symbiotic Bandits"),
  white_panel
  )
#

#draw(PNG("plots/print/symb32AllRegrets.png", 10inch, 6inch), p)
