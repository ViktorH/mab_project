#Plots algorithms for 8 normal bandits.
using JMab
using DataFrames
using Gadfly
using JLD

const nArms = 8
const plays = 1000
const sample = 1
const asamp = 1000

symb8Single = load("./plots/regrets/symbioticBandits/singleAlgo/symb8Single.jld")
symb8Tree = load("./plots/regrets/symbioticBandits/treeNet/symb8Tree.jld")
symb8Flat = load("./plots/regrets/symbioticBandits/flatNet/symb8Flat.jld")

rppdfBayes = JMab.addTotalCuRegret(symb8Single["rppdfBayes"])
rppdfBayes2 = JMab.addTotalCuRegret(symb8Single["rppdfBayes2"])

rppdfBiSortTSTree = JMab.addTotalCuRegret(symb8Tree["rppdfBiSortTSTree"])
rppdfOLTree = JMab.addTotalCuRegret(symb8Tree["rppdfOLTree"])
rppdfBiTree = JMab.addTotalCuRegret(symb8Tree["rppdfBiTree"])

rppdfCliqueNet = JMab.addTotalCuRegret(symb8Flat["rppdfCliqueNet"])
rppdfLocGlobNetRelW = JMab.addTotalCuRegret(symb8Flat["rppdfLocGlobNetRelW"])
rppdfLocGlobNetTS = JMab.addTotalCuRegret(symb8Flat["rppdfLocGlobNetTS"])

#Combine and plot all algorithms
rppdf = vcat(
  rppdfBayes,
  #rppdfBayes2,
  rppdfOLTree,
  rppdfBiTree,
  rppdfBiSortTSTree,
  rppdfCliqueNet,
  rppdfLocGlobNetRelW,
  rppdfLocGlobNetTS,
  )

white_panel = Theme(
  #panel_fill="white",
  background_color="white",
  panel_opacity=1,
  grid_color="black",
  minor_label_color="black",
  #minor_label_font_size=15pt,
  #point_label_font_size=15pt,
  #line_width=2pt
  #major_label_font_size=20pt,
  #key_label_font_size=10pt,
  )

p = plot(rppdf, x=:x, y=:y, color=:label,
  Geom.line,
  #Geom.point,
  #layer(Geom.smooth),
  Guide.YLabel("Regret %"),
  Guide.XLabel("Plays"),
  Guide.Title("Regret % per Play over $nArms Symbiotic Bandits"),
  white_panel
  )
#

#draw(PNG("plots/print/symb8AllRegrets.png", 10inch, 6inch), p)
