#Plots algorithms for 8 normal bandits.
using JMab
using DataFrames
using Gadfly
using JLD

const nArms = 32
const plays = 2000
const sample = 1
const asamp = 1000

#Networked algorithms
const algoClick = [JMab.tsampling for i in 1:nArms]
const resClick = [ones(Int,2,nArms) for i in 1:nArms]

# LocGlobNetRelW
rppLocGlobNet = JMab.symbSampler(asamp,nArms,JMab.locGlobNetRelW, resClick, plays, sample,
  JMab.trialCliqueNet,algoClick)
rppdfLocGlobNetRelW = DataFrame(x = 1:plays, y = rppLocGlobNet, label = "LocGlobNetRelW")

# LocGlobNetTS
rppLocGlobNetTS = JMab.symbSampler(asamp,nArms,JMab.locGlobNetTS, resClick, plays, sample,
  JMab.trialCliqueNet,algoClick)
rppdfLocGlobNetTS = DataFrame(x = 1:plays, y = rppLocGlobNetTS, label = "LocGlobNetTS")

# CliqueNet
rppCliqueNet = JMab.symbSampler(asamp,nArms,JMab.cliqueNet, resClick, plays, sample,
  JMab.trialCliqueNet,algoClick)
rppdfCliqueNet = DataFrame(x = 1:plays, y = rppCliqueNet, label = "CliqueNet")

#Combine and plot all algorithms
rppdf = vcat(
  rppdfCliqueNet,
  rppdfLocGlobNetRelW,
  rppdfLocGlobNetTS,
  )

p = plot(rppdf, x=:x, y=:y, color=:label,
  Geom.line,
  #Geom.point,
  #layer(Geom.smooth),
  Guide.YLabel("Regret %"),
  Guide.XLabel("Plays"),
  Guide.Title("Regret % per Play over $nArms Symbiotic Bandits"),
  #Theme(panel_fill="white")
  )
#

ws = Dict([
  ("rppdfCliqueNet",rppdfCliqueNet),
  ("rppdfLocGlobNetRelW",rppdfLocGlobNetRelW),
  ("rppdfLocGlobNetTS",rppdfLocGlobNetTS),
  ])
#

#rppdfs = load("./plots/regrets/binaryBandits/flatNet/b128flat.jld")
save("./plots/regrets/symbioticBandits/flatNet/symb32Flat2k.jld", ws)
#draw(PNG("plots/print/32bRelWVsNetTS10k.png", 10inch, 6inch), p)
