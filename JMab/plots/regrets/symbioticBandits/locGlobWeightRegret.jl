#locGlob weighting test
using JMab
using DataFrames
using Gadfly

const nArms = 128
const plays = 2000
const sample = 1
const asamp = 1000

#LocGlobNetRel algorithm
const algoClick = [JMab.tsampling for i in 1:nArms]
const resClick = [ones(Int,2,nArms) for i in 1:nArms]
rppLocGlobNetRel = JMab.basicSampler(asamp,nArms,JMab.locGlobNetRelW, resClick, plays, sample,
  JMab.trialCliqueNet,algoClick)
rppdfLocGlobNetRel = DataFrame(x = 1:plays, y = rppLocGlobNetRel, label = "LocGlobNetRelW")

#LocGlobNetTS algorithm
const algoClick = [JMab.tsampling for i in 1:nArms]
const resClick = [ones(Int,2,nArms) for i in 1:nArms]
rppLocGlobNetTS = JMab.basicSampler(asamp,nArms,JMab.locGlobNetTS, resClick, plays, sample,
  JMab.trialCliqueNet,algoClick)
rppdfLocGlobNetTS = DataFrame(x = 1:plays, y = rppLocGlobNetTS, label = "LocGlobNetTS")

#LocGlobNetTS algorithm
const algoClick = [JMab.tsampling for i in 1:nArms]
const resClick = [ones(Int,2,nArms) for i in 1:nArms]
rppLocGlobNetTS = JMab.basicSampler(asamp,nArms,JMab.locGlobNetTS, resClick, plays, sample,
  JMab.trialCliqueNet,algoClick)
rppdfLocGlobNetTS = DataFrame(x = 1:plays, y = rppLocGlobNetTS, label = "LocGlobNetTS")

#LocGlobNetWeight algorithm
const algoClick = [JMab.tsampling for i in 1:nArms]
const resClick = [ones(Int,2,nArms) for i in 1:nArms]
rppLocGlobNetWeigth = JMab.basicSampler(asamp,nArms,JMab.locGlobNetWeight, resClick, plays, sample,
  JMab.trialCliqueNet,algoClick)
rppdfLocGlobNetWeight = DataFrame(x = 1:plays, y = rppLocGlobNetWeigth, label = "LocGlobNetWeight")

# Bayesian Bandit
rppBayes = JMab.basicSampler(asamp,nArms,JMab.bayesB, ones(Int,2,nArms), plays, sample,
  JMab.trialSimpleAlgo)
rppdfBayes = DataFrame(x = 1:plays, y = rppBayes, label = "Bayes")

rppBayes2 = JMab.basicSampler(asamp,nArms,JMab.bayesB2, ones(Int,2,nArms), plays, sample,
  JMab.trialSimpleAlgo)
rppdfBayes2 = DataFrame(x = 1:plays, y = rppBayes2, label = "Bayes2")

#Combine and plot all algorithms
rppdf = vcat(
  rppdfBayes,
  rppdfBayes2,
  #rppdfLocGlobNetRel,
  #rppdfLocGlobNetTS,
  #rppdfLocGlobNetWeight,
  #rppdfBayes,
  )

p = plot(rppdf, x=:x, y=:y, color=:label,
  Geom.line,
  #Geom.point,
  #layer(Geom.smooth),
  Guide.YLabel("Regret"),
  Guide.XLabel("Plays"),
  Guide.Title("Regret per Play on LocGlob Weight differences")
  )
#

#draw(PNG("plots/print/128bsqareBayesVsBayesVsRelW.png", 10inch, 6inch), p)
