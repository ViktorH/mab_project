#Plots algorithms for 8 normal bandits.
using JMab
using DataFrames
using Gadfly
using JLD

const nArms = 128
const plays = 8000
const sample = 1
const asamp = 500

# Bayesian Bandit
rppBayes = JMab.symbSampler(asamp,nArms,JMab.bayesB, ones(Int,2,nArms), plays, sample,
  JMab.trialSimpleAlgo)
rppdfBayes = DataFrame(x = 1:plays, y = rppBayes, label = "Bayes")

# Squared Bayesian Bandit
rppBayes2 = JMab.symbSampler(asamp,nArms,JMab.bayesB2, ones(Int,2,nArms), plays, sample,
  JMab.trialSimpleAlgo)
rppdfBayes2 = DataFrame(x = 1:plays, y = rppBayes2, label = "Bayes2")

#Combine and plot all algorithms
rppdf = vcat(
  rppdfBayes,
  #rppdfBayes2,
  )

p = plot(rppdf, x=:x, y=:y, color=:label,
  #Geom.line,
  #Geom.point,
  layer(Geom.smooth),
  Guide.YLabel("Regret"),
  Guide.XLabel("Plays"),
  Guide.Title("Regret per Play on $nArms Symbiotic Bandits")
  )
#
ws = Dict([
  ("rppdfBayes",rppdfBayes),
  ("rppdfBayes2",rppdfBayes2),
  ])
#
save("./plots/regrets/SymbioticBandits/singleAlgo/symb32Single2k.jld", ws)
#draw(PNG("plots/print/derp.png", 10inch, 6inch), p)
