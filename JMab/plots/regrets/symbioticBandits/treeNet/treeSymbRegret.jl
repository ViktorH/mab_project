#Plots algorithms for 8 normal bandits.
using JMab
using DataFrames
using Gadfly
using JLD

const nArms = 128
const plays = 10000
const sample = 1
const asamp = 1000

# Tree networks
algoBi421 = JMab.defaultBiTree(ones(nArms),JMab.tsampling)
resBi421 = JMab.biTreeBuilder(algoBi421,ones(nArms))
# BiTSSortSplit
rppBiSortTSTree = JMab.symbSampler(asamp,nArms,JMab.binerySplitNest, resBi421, plays, sample,
  JMab.trialBiSort,algoBi421)
rppdfBiSortTSTree = DataFrame(x = 1:plays, y = rppBiSortTSTree, label = "BiSortTSTree")
#describe(rppdfBiSortTSTree)

# Normal Bi Tree
rppBiTree = JMab.symbSampler(asamp,nArms,JMab.binerySplitNest, resBi421, plays, sample,
  JMab.trialBiNest,algoBi421)
rppdfBiTree = DataFrame(x = 1:plays, y = rppBiTree, label = "BiTree")

# Overlapping Tree
algoOL = JMab.defaultBiTree(ones(nArms),JMab.tsampling)
resOL = JMab.treeBuilder(algoOL,ones(nArms))
rppOLTree = JMab.symbSampler(asamp,nArms,JMab.nester, resOL, plays, sample,
  JMab.trialSieve,algoOL)
rppdfOLTree = DataFrame(x = 1:plays, y = rppOLTree, label = "OLTree")

# Collective Tree
algoCol = JMab.defaultBiTree(ones(nArms),JMab.tsampling)
resCol = JMab.treeBuilder(algoCol,ones(nArms))
rppColTree = JMab.symbSampler(asamp,nArms,JMab.lgNester, resCol, plays, sample,
  JMab.trialSieve,algoCol)
#cumuColTree = round(sum(rppColTree),2)
rppdfColTree = DataFrame(x = 1:plays, y = rppColTree, label = "ColTree")

#Combine and plot all algorithms
rppdf = vcat(
  rppdfBiTree,
  rppdfOLTree,
  rppdfBiSortTSTree,
  rppdfColTree,
  )

p = plot(rppdf, x=:x, y=:y, color=:label,
  Geom.line,
  #Geom.point,
  #layer(Geom.smooth),
  Guide.YLabel("Regret %"),
  Guide.XLabel("Plays"),
  Guide.Title("Regret % per Play over $nArms Symbiotic Bandits"),
  #Theme(panel_fill="white")
  )
#

ws = Dict([
  ("rppdfBiTree",rppdfBiTree),
  ("rppdfOLTree",rppdfOLTree),
  ("rppdfBiSortTSTree",rppdfBiSortTSTree),
  ("rppdfColTree",rppdfColTree),
  ])
#

#rppdfs = load("./plots/regrets/binaryBandits/flatNet/b128flat.jld")
save("./plots/regrets/symbioticBandits/treeNet/symb32Tree2k.jld", ws)
#draw(PNG("plots/print/128bFlatNets.png", 10inch, 6inch), p)
