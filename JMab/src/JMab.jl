__precompile__()
#Mab
#include("C:\\Users\\VH\\Documents\\Code\\MAB_Project\\Julia_mab\\src\\bandits\\binarybandit.jl")
#Pkg.add("DataFrames")
module JMab
  #Bandits
  include("bandits/binarybandit.jl")
  include("bandits/symbioticbandit.jl")

  #Algorithms
  include("algo/algo.jl")
  include("algo/egreedy.jl")
  include("algo/bayesb.jl")
  include("algo/ucb.jl")
  include("algo/layeralgos/nestedb.jl")
  include("algo/layeralgos/binarysplitnest.jl")
  include("algo/layeralgos/sortingbinarytree.jl")
  include("algo/layeralgos/layersetup.jl")
  include("algo/networkalgos/cliquenet.jl")
  include("algo/networkalgos/locGlobNet.jl")

  #Measuring functions for pre Plotting
  include("measure/measure.jl")
  include("measure/optimalArm.jl")
  include("measure/regret.jl")
end
