#shared algorithm functions

#Updates a spesific matrix in a array, at the given level and horizion
function updateResults(results,level,horiz,bestBandit,pickedBandit)
  newResults = deepcopy(results)
  newResults[level][horiz][1,bestBandit] = results[level][horiz][1,bestBandit] + pickedBandit
  newResults[level][horiz][2,bestBandit] = results[level][horiz][2,bestBandit] + 1 - pickedBandit
  return newResults
end

#Sort tree by bandits. TODO revamp when done in measuretest.jl
function banditSort(tree)
  bandits = bcat(tree)
  bsamples = JMab.randomSamples(bandits) #TODO only sorts the sample output.
  sortTree = sort(bsamples)
  return sortTree
end

#horizontal concatination of first level bandit tree.
function bcat(mat)
  newMat = mat[1][1]
  for i in 2:length(mat[1])
    newMat = hcat(newMat,mat[1][i])
  end
  return newMat
end

#TODO altough binary split works now, Bi21 setup does not work, it might be this function.
function findBandit(bandit,horis)
  res = horis*2
  if isodd(bandit)
    res = res-1
  end
  return res
end
