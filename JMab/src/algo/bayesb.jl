#Bayesian bandit(Thompson sampling)
using Distributions

function bayesB(results, arms)
  samples = randomSamples(results)
  pick = indmax(samples)
  trial = arms[pick]()
  newResults = copy(results)
  newResults[1,pick] = results[1,pick] + trial
  newResults[2,pick] = results[2,pick] + 1 - trial
  return newResults
end

function bayesB2(results, arms)
  samples = randomSamples(results.^2)
  pick = indmax(samples)
  trial = arms[pick]()
  newResults = copy(results)
  newResults[1,pick] = results[1,pick] + trial
  newResults[2,pick] = results[2,pick] + 1 - trial
  return newResults
end

#Thompson Sampling
function tsampling(results)
  samples = randomSamples(results)
  return indmax(samples)
end

#Squared Thompson Sampling
function sqtsampling(results)
  samples = randomSamples(results.^2)
  return indmax(samples)
end

#Draws random samples from a beta Distributions.
function randomSamples(results)
  samples = zeros(length(results[1,:]))
  for i in eachindex(samples)
    samples[i] = rand(Beta(results[1,i],results[2,i]))
  end
  return samples
end
