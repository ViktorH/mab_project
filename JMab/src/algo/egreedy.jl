#e-greedy
function eGreedy(results::Matrix, arms::Array, explorePr = 0.1)
  if rand(1)[1] <= explorePr
    #explore
    pick = rand(1:length(arms))
    trial = arms[pick]()
  else
    #exploit
    winfail = Array{Float64}(length(arms))
    for i in eachindex(arms)
      winfail[i] = results[1,i]/results[2,i]
    end
    pick = pickBandit(winfail)
    trial = arms[pick]()
  end
  newResults = copy(results)
  newResults[1,pick] = results[1,pick] + trial
  newResults[2,pick] = results[2,pick] + 1 - trial
  return newResults
end

#Picks randomly between all best scoring bandits
function pickBandit(winfail)
  maxIndex = indexin(winfail,[maximum(winfail)])
  indexes = zeros(Int,length(maxIndex))
  for i in eachindex(maxIndex)
    if maxIndex[i] == 1
      indexes[i] = i
    end
  end
  rand(filter(x-> x > 0,indexes))
end

function exploreExploit(results,arms,explorePr = 0.1)
  if rand(1)[1] <= explorePr
    #explore
    pick = rand(1:length(arms))
  else
    #exploit
    winfail = Array{Float64}(length(arms))
    for i in eachindex(arms)
      winfail[i] = results[1,i]/results[2,i]
    end
    pick = pickBandit(winfail)
  end
  return pick
end

function greed(results)
  samples = zeros(length(results[1,:]))
  for i in eachindex(samples)
    samples[i] = results[1,i]/results[2,i]
  end
  return samples
end

function epsilonGreedy(results, epsilon = 0.1)
  if rand(1)[1] <= epsilon
    pick = rand(1:length(results[1,:]))
  else
    pick = pickBandit(greed(results))
  end
  return pick
end
