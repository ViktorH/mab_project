#binarySplitNest

function bineryLvl1Split(results, arms, level = 1,horiz = 1,algo = nothing)
  #Bandit version
  if level == 1
    res = results[level][horiz]
    bestBandit = algo[level][horiz](res)
    pickedBandit = arms[findBandit(bestBandit,horiz)]()
    after = deepcopy(results)
    #println("B",bestBandit)

  #Algo verions
  else
    sublvl = level -1
    res = results[level][horiz]
    #ar = algo[sublvl]
    bestBandit = algo[level][horiz](res)

    before = copy(results[sublvl][bestBandit])
    after = binerySplitNest(results,arms,sublvl,bestBandit,algo)
    if before[1,:] != after[sublvl][bestBandit][1,:]
      pickedBandit = 1
    else
      pickedBandit = 0
    end
    #println(before[1,:],after[sublvl][bestBandit][1,:])
  end

  #println("L",level,"H",horiz)
  newResults = JMab.updateResults(after,level,horiz,bestBandit,pickedBandit)
  return newResults
end


function binerySplitNest(results, arms, level = 1,horiz = 1,algo = nothing)
  #Bandit version
  if level == 1
    res = results[level][horiz]
    bestBandit = algo[level][horiz](res)
    pickedBandit = arms[findBandit(bestBandit,horiz)]()
    after = deepcopy(results)
    #println("B",bestBandit)

  #Algo verions
  else
    sublvl = level -1
    res = results[level][horiz]
    bestBandit = algo[level][horiz](res)

    before = copy(results[sublvl][findBandit(bestBandit,horiz)])
    after = binerySplitNest(results,arms,sublvl,findBandit(bestBandit,horiz),algo)
    if before[1,:] != after[sublvl][findBandit(bestBandit,horiz)][1,:]
      pickedBandit = 1
    else
      pickedBandit = 0
    end
    #println(before[1,:],after[sublvl][bestBandit][1,:])
  end

  #println("L",level,"H",horiz)
  newResults = JMab.updateResults(after,level,horiz,bestBandit,pickedBandit)
  return newResults
end
