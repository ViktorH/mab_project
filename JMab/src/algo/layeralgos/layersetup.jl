#layer setup

"""given a tree of algos, creates a matching tree of matrixes."""
function treeBuilder(algoTree,bandits)
  matrixTree = Array{Array}(length(algoTree))
  for i in eachindex(algoTree)
    matrixTree[i] = Array{Array}(length(algoTree[i]))
    for n in eachindex(matrixTree[i])
      if i == 1
        matrixSize = length(bandits)
      else
        matrixSize = length(matrixTree[i-1])
      end
      matrixTree[i][n] = ones(Int,2,matrixSize)
    end
  end
  return matrixTree
end

#Divindes bandits over level 1 algorithms,NOTE bandit/lvl1 can not have remainders.
function biTreeBuilder(algoTree,bandits)
  matrixTree = Array{Array}(length(algoTree))
  for i in eachindex(algoTree)
    matrixTree[i] = Array{Array}(length(algoTree[i]))
    for n in eachindex(matrixTree[i])
      if i == 1
        matrixSize = length(bandits)/length(matrixTree[i])
      else
        matrixSize = (length(matrixTree[i-1])/length(matrixTree[i]))
      end
      matrixTree[i][n] = ones(Int,2,convert(Int,matrixSize))
    end
  end
  return matrixTree
end

function findDepth(bandits)
  x = length(bandits)
  y = 1
  while x/2 % 2 == 0
    x = x/2
    y = y +1
  end
  return y
end

function fillBiTree(levels,bandits)
  tree = Array{Array}(levels)
  b = length(bandits)
  for n in 1:levels
    b = b/2
    tree[n] = Array{Any}(trunc(Int,b))
  end
  return tree
end

#Creates a default mab layer tree for hierarchical algos.
function defaultBiTree(bandits,algoOrMat)
  z = JMab.findDepth(bandits)
  zz = JMab.fillBiTree(z,bandits)
  for n in 1:length(zz)
    for i in 1:length(zz[n])
      zz[n][i] = algoOrMat
    end
  end
  return zz
end
