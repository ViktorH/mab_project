#Naive Nested algorithm

#looks through a tree of algos and bandits, and updates current results.
function nester(results, arms, level = 1,horiz = 1,algo = nothing)
  #Bandit version
  if level == 1
    res = results[level][horiz]
    bestBandit = algo[level][horiz](res)
    pickedBandit = arms[bestBandit]()
    after = deepcopy(results)
  #Algo verions
  else
    sublvl = level -1
    res = results[level][horiz]
    #ar = algo[sublvl]
    bestBandit = algo[level][horiz](res)
    before = copy(results[sublvl][bestBandit])
    after = nester(results,arms,sublvl,bestBandit,algo)
    if before[1,:] != after[sublvl][bestBandit][1,:]
      pickedBandit = 1
    else
      pickedBandit = 0
    end
  end
  newResults = updateResults(after,level,horiz,bestBandit,pickedBandit)
  return newResults
end

#looks through a tree of algos and bandits, and updates current results.
function lgNester(results, arms, level = 1,horiz = 1,algo = nothing)
  #Bandit version
  if level == 1
    res = results[level][horiz]

    collective = sum(results[level]) - length(results[level]) +1
    weightedColl = collective .* res

    bestBandit = algo[level][horiz](weightedColl)
    pickedBandit = arms[bestBandit]()
    after = deepcopy(results)
  #Algo verions
  else
    sublvl = level -1
    res = results[level][horiz]

    collective = sum(results[level]) - length(results[level]) +1
    weightedColl = collective .* res
    bestBandit = algo[level][horiz](weightedColl)


    before = copy(results[sublvl][bestBandit])
    after = lgNester(results,arms,sublvl,bestBandit,algo)
    if before[1,:] != after[sublvl][bestBandit][1,:]
      pickedBandit = 1
    else
      pickedBandit = 0
    end
  end
  newResults = updateResults(after,level,horiz,bestBandit,pickedBandit)
  return newResults
end
