#Sorting version of binarysplitNest.jl
#TODO there is a issue with sorted bandits not getting passed around.
function sorter(tree, arms, level = 1,horiz = 1,algo = nothing)
  sortedtree = JMab.sortBinaryTree(tree, arms)
  newtree = JMab.binerySplitNest(sortedtree[1], sortedtree[2], level, horiz, algo)
  return newtree
end

#TODO not quite done, as we are passing extra numbers upwards.
function sortBinaryTree(tree,bandits)
  matrixcat = JMab.bcat(tree)
  randSamp = JMab.randomSamples(matrixcat) #change for dynamic MAB sorter(UCB,egreedy etc)
  indsort = JMab.indexsort(randSamp)

  matbandsort = JMab.matrixBanditSort(tree,matrixcat,indsort)
  sortedband = JMab.banditSort(bandits,indsort)

  sortedtree = JMab.passUpward(matbandsort)
  return sortedtree, sortedband
end

function banditSort(bandits,indsort)
  nbandits = Array{Function}(length(bandits))
  for i in 1:length(bandits)
    nbandits[i] = bandits[indsort[i]]
  end
  return nbandits
end

function matrixBanditSort(tree,matrixcat,indsort)
  ntree = deepcopy(tree)
  for n in 1:length(tree[1])
    for i in 1:2
      maxind = indsort[JMab.findBandit(i,n)]
      ntree[1][n][i*2-1:i*2] = matrixcat[JMab.findBandit(1,maxind):JMab.findBandit(2,maxind)]
    end
  end
  return ntree
end

function passUpward(tree)
  tree = tree -1
  for n in 2:length(tree)
    for i in 1:length(tree[n])
      tree[n][i][1:2] = tree[n-1][i*2-1][1:2] + tree[n-1][i*2-1][3:4] #NOTE only works for binary trees.
      tree[n][i][3:4] = tree[n-1][i*2][1:2] + tree[n-1][i*2][3:4]
    end
  end
  return tree +1
end

function indexsort(t)
  coll = Array{Any}(length(t))
  for i in eachindex(t)
    coll[i] = indmax(t)
    t[indmax(t)] = 0
  end
  return coll
end
