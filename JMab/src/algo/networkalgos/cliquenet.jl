#ClickNetwork mabs.

function cliqueNet(results::Array,arms::Array,algos::Array,currentpos = 1)
  nextpos = algos[currentpos](results[currentpos])
  sucfail = arms[nextpos]()
  results[currentpos][1,nextpos] = results[currentpos][1,nextpos] + sucfail
  results[currentpos][2,nextpos] = results[currentpos][2,nextpos] + 1 - sucfail
  newresult = deepcopy(results)
  return newresult,nextpos
end
