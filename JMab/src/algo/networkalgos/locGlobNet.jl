#Clique Network, that alters between the use of local information vs global.

#NOTE GOOD
function locGlobNetRelW(results,arms,algos,currentpos = rand(1:length(arms)))
  nextpos = locRelWeight(results,algos,currentpos)
  return updateNetRes(nextpos,results,arms,currentpos)
end

#NOTE GOOD Is better at symbBandit in the long run
function locGlobNetTS(results,arms,algos,currentpos = rand(1:length(arms)))
  nextpos = locTSWeight(results,algos,currentpos)
  return updateNetRes(nextpos,results,arms,currentpos)
end

#NOTE BAD
function locGlobNetWeight(results,arms,algos,currentpos = rand(1:length(arms)))
  nextpos = locweight(results,algos,currentpos)
  return updateNetRes(nextpos,results,arms,currentpos)
end

function updateNetRes(nextpos,results,arms,currentpos)
  sucfail = arms[nextpos]()
  results[currentpos][1,nextpos] = results[currentpos][1,nextpos] + sucfail
  results[currentpos][2,nextpos] = results[currentpos][2,nextpos] + 1 - sucfail
  newresult = deepcopy(results)
  return newresult, nextpos
end

function collectglob(results::Array, algos, currentpos)
  collective = sum(results) - length(results) + 1
  locGlob = hcat(results[currentpos], collective)
  nextpos = algos[currentpos](locGlob)
  if nextpos > length(results[1][1,:])
    nextpos = trunc(Int, nextpos/2)
  end
  return nextpos
end

#NOTE BAD
function locweight(results::Array, algos::Array, currentpos::Int)
  collective = sum(results) - length(results) + 1
  weightedColl = collective + results[currentpos] - 1
  nextpos = algos[currentpos](weightedColl)
  return nextpos
end

#NOTE GOOD
function locRelWeight(results::Array, algos::Array, currentpos::Int)
  collective = sum(results) - length(results) + 1
  weightedColl = collective .* results[currentpos] #-1
  nextpos = algos[currentpos](weightedColl)
  return nextpos
end

#NOTE GOOD
function locTSWeight(results, algos, currentpos)
  collective = JMab.randomSamples(sum(results) - length(results)+1)
  weights = JMab.randomSamples(results[currentpos])
  weightedColl = collective .* weights
  nextpos = indmax(weightedColl)
  return nextpos
end

function locTS(results, algos, currentpos)
  collective = JMab.randomSamples(sum(results) - length(results)+1)
  weights = JMab.randomSamples(results[currentpos])
  weightedColl = sqrt(collective .* weights)
  nextpos = indmax(weightedColl)
  return nextpos
end
