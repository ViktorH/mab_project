#Upper bound confidence
#https://jeremykun.com/2013/10/28/optimism-in-the-face-of-uncertainty-the-ucb1-algorithm/
#ucbs = [payoffSums[i] / numPlays[i] + upperBound(step, numPlays[i])

#numplays = trails, times the current arm is played
#step = total amount of runs of the algo.
upperbound(step, numPlays) = sqrt(2 * log(step + 1) / numPlays)

#value = rewardsum/trail + upperbound(step,trail)
#max(value)
function ucb1(results)
  samples = zeros(length(results[1,:]))
  for i in eachindex(samples)
    samples[i] = results[1,i]/results[2,i] + upperbound(sum(results),results[2,i] + results[1,i])
    #samples[i] = results[1,i]/results[2,i] + upperbound(sum(res[2,:]),results[2,i])
  end #when tie, pick leftest arm.
  return indmax(samples)
end

function ucb1(results, arms)
  samples = zeros(length(arms))
  for i in eachindex(arms)
    samples[i] = results[1,i]/results[2,i] + upperbound(sum(results),results[2,i] + results[1,i])
    #samples[i] = results[1,i]/results[2,i] + upperbound(sum(results[2,:]),results[2,i])
  end
  pick = indmax(samples)
  trial = arms[pick]()
  newResults = copy(results)
  newResults[1,pick] = results[1,pick] + trial
  newResults[2,pick] = results[2,pick] + 1 - trial
  return newResults
end
