#A bandit returning [1,0] depending on some given probability

#Standard distribution binominal reward
function binaryBandit(pr)
  if rand(1)[1] <= pr
    return 1
  end
  return 0
end

#Creates an array of bandit functions
function setNbandits(nbandits::Array{Float64})
  arms = map(x -> function () binaryBandit(x) end, nbandits)
  return arms
end

#Creates N amount bandits,TODO with Y variance "",variance::Float64=1"
function generateNBandits(nbandits::Int)
  prob = rand(nbandits)
  print(prob)
  arms = setNbandits(prob)
  return arms
end
