#symbiotic bandits, binary version
#playing one bandit effect the pr(success) of another

function bonusmod(x::Array)
  global bonusstate = x
end

function symbBandit(pr::Float64, bandnr::Int, symbiosis::Dict, prBoost = 0.2)
  for n in 1:length(bonusstate)
    if bonusstate[n] == bandnr
      pr = pr + prBoost
      newstate = filter(x -> x != bandnr, bonusstate)
      if haskey(symbiosis, bandnr)
        global bonusstate = vcat(newstate, symbiosis[bandnr])
      else
        global bonusstate = newstate
      end
      break #breaks at first find.
    end
  end
  return JMab.binaryBandit(pr)
end

createSymBandit(pr::Float64,index::Int,dictSymb::Dict,prBoost::Float64) =
  function () JMab.symbBandit(pr,index,dictSymb,prBoost) end

function setNSymbandits(armsPr::Array{Float64}, dictSymb = Dict([(1,[2,3]),(2,[1]),(3,[1])]), prBoost = 0.2)
  bandits = Array{Function}(length(armsPr))
  for i in 1:length(armsPr)
    bandits[i] = createSymBandit(armsPr[i],i,dictSymb,prBoost) #JMab.symbBandit(armsPr[i],i,dictSymb)
  end
  return bandits
end
