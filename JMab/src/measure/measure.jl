#Measure
#NOTE Trial functions runs and records the algorithms by N plays and Samples.
#trial functions should return a array(sample) of arrays(plays)

function trialSimpleAlgo(algo, results, arms, plays, sample, algolist = nothing)
  trialResults = Array{Array}(sample)
  for n in 1:sample
    res = Array{Matrix}(plays)
    mat = copy(results)
    for i in 1:plays # Iterate playes
      mat = algo(mat,arms)
      res[i] = mat
    end
    trialResults[n] = res
  end
  return trialResults -1 #remove initial value
end

#TODO make trial work on nester() aswell as normal algos
#alg = JMab.nester
#trial function for sieve structures.
function trialSieve(algo, resultsList, arms, plays, sample, algoList = nothing)
  trialResults = Array{Array}(sample)
  for n in 1:sample
    res = Array{Matrix}(plays)
    mat = deepcopy(resultsList)
    for i in 1:plays
      mat = algo(mat,arms,length(resultsList),1,algoList)
      newMat = sum(mat[1])
      res[i] = newMat
    end
    trialResults[n] = res
  end
  return trialResults -length(resultsList[1])
end

#Trial function for binary nested structures.
function trialBiNest(algo, resultsList, arms, plays, sample, algoList = nothing)
  trialResults = Array{Array}(sample)
  for n in 1:sample
    res = Array{Matrix}(plays)
    mat = deepcopy(resultsList)
    for i in 1:plays
      mat = algo(mat,arms,length(resultsList),1,algoList)
      newMat = mat[1][1]
      for z in 2:length(mat[1])
        newMat = hcat(newMat,mat[1][z])
      end
      res[i] = newMat
    end
    trialResults[n] = res
  end
  return trialResults -1
end

#Trial function for binary SORT nested structures.
function trialBiSort(algo, resultsList, arms, plays, sample, algoList = nothing)
  trialResults = Array{Array}(sample)
  for n in 1:sample
    #arms = JMab.rearrange(arms)
    res = Array{Matrix}(plays)
    mat = deepcopy(resultsList)
    for i in 1:plays

      if i % 1 == 0
        sorts = JMab.sortBinaryTree(mat,arms)
        mat = sorts[1]
        arms = sorts[2]
      end

      mat = algo(mat, arms, length(resultsList), 1, algoList)
      newMat = mat[1][1]
      for z in 2:length(mat[1])
        newMat = hcat(newMat,mat[1][z])
      end
      res[i] = newMat
    end
    trialResults[n] = res
  end
  return trialResults -1
end

#Trial function for Clique structures.
function trialCliqueNet(algo, resultsList, arms, plays, sample, algoList = nothing)
  trialResults = Array{Array}(sample)
  for n in 1:sample
    res = Array{Matrix}(plays)
    mat = [deepcopy(resultsList),1]
    for i in 1:plays
      mat = algo(mat[1], arms, algoList, mat[2])
      res[i] = sum(mat[1])
    end
    trialResults[n] = res
  end
  return trialResults -length(arms)
end


function rearrange(t)
  coll = Array{Any}(length(t))
  for i in eachindex(t)
    pick = rand(t)
    coll[i] = pick
    t = filter((x -> x != pick),t) #NOTE Bandits are threated equal if they have equal Pr end
  end
  return coll
end
