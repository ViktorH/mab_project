#OptimalArm

#Creates a array of the prosentage of which the optimal arm is playd at each trial
function optimalArmPlays(trialResults,arms,plays)
  optimalPlays = Array{Float64}(plays)
  for i in eachindex(optimalPlays)
    optimalPlays[i] = (trialResults[i][1,indmax(arms)] + trialResults[i][2,indmax(arms)])/
    sum(trialResults[i])
  end
  return optimalPlays
end

function optimalArmFunc(algo, armsPr = [0.1,0.2,0.3], plays = 1000, sample = 1000)
  bandits = JMab.setNbandits(armsPr) #TODO might be exportet out later
  initial = ones(Int,2,length(bandits)) #TODO might be exportet out later
  res_m = trial(algo, initial, bandits, plays,sample)#Run algo' over armsPr', plays' amount of times
  res_m = removeOne(res_m)#Removes 1 point initial value.
  optimalArmResults_v = optimalArmPlays(mean(res_m),armsPr,plays)#Optimal arm plays / all arm plays
end

#optimal arm for nested algorithm structures
function optimalArmFunc2(algo,initial,armsPr, plays, sample,algoList = nothing)
  bandits = JMab.setNbandits(armsPr) #TODO might be exportet out later
  res_m = trial2(algo,initial,bandits,plays,sample,algoList)#Run algo' over armsPr', plays' amount of times
  res_m = res_m -length(initial[1])
  optimalArmResults_v = optimalArmPlays(mean(res_m),armsPr,plays)#Optimal arm plays / all arm plays
end
