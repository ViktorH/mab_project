#RegretPerPl

#calculate mean agent reward per step
function meanAgentReward(results)
  meanReward = Array{Int}(length(results))
  previousReward = 0
  for i in eachindex(meanReward)
    reward = sum(results[i][1,:])#NOTE results[i] might have to change
    meanReward[i] = reward - previousReward
    previousReward = copy(reward)
  end
  return meanReward
end

#Calculate mean optimal reward per step
function meanOptimalReward(armsPr,plays,sample)
  bandits = JMab.setNbandits(armsPr)
  optimalPerformance = zeros(Int,plays)
  for i in 1:plays
    posX = Array{Int}(sample)
    for n in 1:sample
      posX[n] = bandits[indmax(armsPr)]()#NOTE indmax inside loop might be smart for symbBandits later
    end
    optimalPerformance[i] = sum(posX)
  end
  return optimalPerformance
end

#a generelized version of regret
function regretPerPlayREFAC(algo,initial,armsPr,plays,sample,trialType,bandits,algoList = nothing)
  trialResults = trialType(algo,initial,bandits,plays,sample,algoList)#Run algo, N times, create X amount of samples
  regretm = calcRegret(trialResults,armsPr,plays,sample)
  return regretm
end

function calcRegret(trialResults,armsPr,plays,sample)
  res_m = sum(trialResults)#sum all sample
  endend = JMab.meanAgentReward(res_m)
  #optimalPerformance = meanOptimalReward(armsPr,plays,sample)
  optimal = maximum(armsPr) * sample
  optimalPerformance = repeat([optimal],outer=plays)
  regretm = (optimalPerformance - endend)/sample # (optimal - agent)/optimalPerformance[1]
  return regretm
end

function addTotalCuRegret(df)
  totalRegret = round(sum(df[:,2]),2)
  name = df[1,3]
  newdf = deepcopy(df)
  newdf[:,3] = "$name($totalRegret)"
  return newdf
end


"""Basic Binominal Bandit testcase """
function basicSampler(samps::Int,nArms::Int,algo,initial,plays,sample,trialType,algoList = nothing)
  newres = Array{Array}(samps)
  for i in 1:samps
    armsPr = rand(Distributions.Uniform(0,1),nArms)# set new distribution
    bandits = JMab.setNbandits(armsPr)# create bandits

    newres[i] = JMab.regretPerPlayREFAC(algo,initial,armsPr,plays,sample,trialType,bandits,algoList)
  end
  return sum(newres)/samps
end

"""Symbiotic sampler Bandit testcase """
function symbSampler(samps::Int,nArms::Int,algo,initial,plays,sample,trialType,algoList = nothing)
  newres = Array{Array}(samps)
  for i in 1:samps
    armsPr = rand(Distributions.Uniform(0,0.5),nArms)# set new distribution
    symb1 = rand(1:nArms)
    symb2 = rand(1:nArms)
    bandits = JMab.setNSymbandits(armsPr,Dict([(symb1,[symb2]),(symb2,[symb1])]),0.5)# create bandits
    JMab.bonusmod([symb1])

    newres[i] = JMab.regretPerPlayREFAC(algo,initial,armsPr,plays,sample,trialType,bandits,algoList)
  end
  return sum(newres)/samps
end
