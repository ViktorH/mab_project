#algotests

@testset "MAB Algos" begin
  @testset "e-Greedy" begin
    res = ones(Int,2,3)
    comp = ones(Int,2,3)
    arms = JMab.setNbandits([0.1,0.3,0.2])
    res = JMab.eGreedy(res,arms)
    @test sum(res) > sum(comp)
  end

  @testset "bayesB" begin
    res = ones(Int,2,3)
    comp = ones(Int,2,3)
    arms = JMab.setNbandits([0.1,0.3,0.2])
    res = JMab.bayesB(res,arms)
    @test sum(res) > sum(comp)
  end
end
