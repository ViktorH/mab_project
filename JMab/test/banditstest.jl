#BanditsTest
@testset "Bandits" begin
  @testset "Binominal Bandit" begin
    @test JMab.binaryBandit(1) == 1
    @test JMab.binaryBandit(0) == 0

    z = JMab.binaryBandit(0.5)
    @test z == 1||z == 0
    @inferred JMab.setNbandits([0.1,0.2,0.3])
  end
end
