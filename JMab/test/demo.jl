########

#I shall now sin
#global bonusstate = 1
using JMab
using Distributions
rand(Distributions.Uniform(0,1),16)

rand(1:20,5)/100

using JMab
using DataFrames
using Gadfly
#const armsPr = rand(8) #for 8 random generated armsPr
banditType = JMab.setNbandits
const armsPr = [0.2,0.2,0.1,0.1,0.1,0.1,0.05,0.3]
const plays = 2000
const sample = 2000

#Networked algorithms
algoClick = [JMab.tsampling,JMab.tsampling,JMab.tsampling,JMab.tsampling,JMab.tsampling,JMab.tsampling,JMab.tsampling,JMab.tsampling]
resClick = [ones(Int,2,length(armsPr)) for i in 1:length(armsPr)]

Juno@step JMab.regretPerPlayREFAC(JMab.locGlobNet,resClick,armsPr,plays,sample,JMab.cliqueNetTrial,banditType,algoClick)


using JMab

derp = JMab.createSymBandit(0.5,1,Dict([(1,2),(2,1)]),0.2)

ferp = JMab.setNSymbandits([0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1], Dict([(1,2),(2,1)]), 0.9)
ferp[2]()

lk = [1 1 3 1; 1 1 1 2]
ck = lk + 7
lkck = ck -7





JMab.tsampling(ck)

band = JMab.setNbandits([0.1, 0.1])
res = JMab.bayesB(res, band)

JMab.randomSamples(fk)

nlk = [sum(lk[1,:]);sum(lk[2,:])]
nck = [sum(ck[1,:]);sum(ck[2,:])]
#nck2 = trunc(Int,nck/8)
nck2 = nck/8
res = hcat(nck2,nlk)
JMab.tsampling(res)

#op1: squish ck relativ to lk 1:1 NOTE does not express confidence in information
#op2: squish numbers relative to confidence.Use TS numbers as scalers

#in:prList, choice, out:choiceresult,newstate
#internal

#In a current state, bonuses etc. output newstate
using JMab

#Updates current bonus state
function symbstate(symbiosis::Dict, choice::Int, state::Array)
  newstate = copy(state)
  for i in 1:length(state)
    if choice == state[i]
      filter!(x -> x != choice, newstate)
      newstate = vcat(newstate,symbiosis[choice])
    end
  end
  return newstate
end

function symbBandit(pr::Float64, bandnr::Int, symbiosis::Dict, prBoost = 0.2)
  for n in 1:length(currentstate)
    if currentstate[n] == bandnr
      pr = pr + prBoost
      newstate = filter(x -> x != bandnr, currentstate)
      if haskey(symbiosis, bandnr)
        global currentstate = vcat(newstate, symbiosis[bandnr])
      else
        global currentstate = newstate
      end
      break #breaks at first find.
    end
  end
  return JMab.binaryBandit(pr)
end

keys(symb)
pr = [0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1]
symb = Dict([(1,[2,3]),(2,[1]),(3,[1]),(7,[8]),(8,[7])])
#symb = Dict([(1,2),(2,1),(7,8),(8,7)])
currentstate = [1,7]
#newstate = symbstate(symb,1,currentstate)
newstate = symbBandit(0.1,3,symb,0.9)
#Check if int already exists?




#### NOTE trying to make the use of bandit type more dynamic
#Possible with multiple dispatch
#I am calling a function with 1 or many parameters.
#The super function has to beable to hold 1 or many parameters.
f(x,y,z) = x+y+z
g(x) = x

function derp(func::Function, x)
  res = func(x)
  return res
end

function derp(func::Function, x, y = nothing, z = nothing)
  res = func(x,y,z)
  return res
end

a = JMab.randomSamples([1 1 1 1;1 1 1 1])
b = JMab.randomSamples([1 1 1 1;1 1 1 1])

a + b

using JMab

const armsPr = [0.2,0.2,0.2,0.1,0.1,0.1,0.05,0.3]
symb = Dict([(1,[2,3]),(2,[1]),(3,[1])])
#bandits = JMab.setNSymbandits(armsPr,symb,0.2)
bonusstate
bonusstate = [1]

JMab.symbBandit(0.1,1,symb,0.9)


JMab.randomSamples()
a = [2 1;2 1]
b = [1 1;1 1]

sum(a[1,:])
sum(a[2,:])

[sum(a[1,:]),sum(a[2,:])]

const armsPr = rand(Distributions.Normal(0,1),10000).^2
function f(x)
  if x >= 1.0
    return x -floor(x)
  end
  return x
end
normdisFloor = map(f,armsPr)

using DataFrames
using Gadfly
maximum(normdisFloor)
normdis = DataFrame(x = 1:length(normdisFloor), y = normdisFloor, label = "armsPrDistri")
p = plot(normdis, x=:x, y=:y, color=:label,
  #Geom.line,
  Geom.point,
  #layer(Geom.smooth),
  Guide.YLabel("y"),
  Guide.XLabel("x"),
  Guide.Title("armsPr normDistribution")
  )
#
### JLDSTUFF
Pkg.update("JLD")

using JLD

rppdfs = load("../b128TreesVsFlatNetVsSingle.jld")

rppdfCliqueNet = rppdfs["rppdfCliqueNet"]
rppdfLocGlobNetRelW = rppdfs["rppdfLocGlobNetRelW"]
rppdfLocGlobNetTS = rppdfs["rppdfLocGlobNetTS"]

keys(rppdfs)


#save("../myfile.jld", ws)

ws = Dict([
  ("rppdfBayes", rppdfBayes),
  ("rppdfEG",rppdfEG),
  ("rppdfUCB",rppdfUCB),
  ("rppdfOLTree",rppdfOLTree),
  ("rppdfBiTree",rppdfBiTree),
  ("rppdfBiSortTSTree",rppdfBiSortTSTree),
  ("rppdfBiUCBTree",rppdfBiUCBTree),
  ("rppdfCliqueNet",rppdfCliqueNet),
  ("rppdfLocGlobNetRelW",rppdfLocGlobNetRelW),
  ("rppdfLocGlobNetTS",rppdfLocGlobNetTS),
  ])


Pkg.add("PyCall")
Pkg.add("RCall")

using PyCall
@pyimport math
math.sin(math.pi / 4) - sin(pi / 4)

using RCall
using RDatasets
mtc = dataset("datasets", "mtcars");
library(ggplot2)


Pkg.add("RDatasets")

reval("plot(rnorm(10))")
reval("curve(dbeta(x,20,10))")
reval("curve(x^10, xlim = c(0,100))")
R"var <- 123*321"

@rlibrary("maps")

R"install.packages('ggplot2')"


[i+1 for i in 0:9]


f(x::Int)::Int = print("this is $x")


a = rand(1000)
amean = mean(a)
b = [(i - amean)^2 for i in a]
c = sqrt(mean(b))


b = [(i-amean)^2 for i in a]
sqrt(sum(b)/(length(b)-1))
