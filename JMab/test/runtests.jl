#Run all tests
#Base.runtests([tests=["all"][, numcores=ceil(Integer, Sys.CPU_CORES / 2)]])
using Base.Test
using JMab

include("algotest.jl")
include("banditstest.jl")

#TODO Unittests of all functions
