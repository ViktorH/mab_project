'''
Created on 13. jul. 2016
http://machinelearningmastery.com/naive-bayes-classifier-scratch-python/

@author: Viktor
'''
# Example of Naive Bayes implemented from Scratch in Python
import csv
def loadCsv(filename):
    lines = csv.reader(open(filename, "rb"))
    dataset = list(lines)
    for i in range(len(dataset)):
        dataset[i] = [float(x) for x in dataset[i]]
    return dataset

filename = 'pima-indians-diabetes.csv'
dataset = loadCsv(filename)
print('Loaded data file {0} with {1} rows').format(filename, len(dataset))
