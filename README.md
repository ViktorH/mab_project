﻿# Exploring Multi-Armed Bandit(MAB) solutions with network structures.

The final code is written in Julia, under the JMab folder.
Where the folder JMab/plot/print contains all final printed plots.

This module is made to simulate a MAB scenarios using mainly Thompson Sampling as nodes in a network that tries to solve the Bernoulli MAB problem. This is also experimented in a novel MAB problem called “Symbiotic bandits”; where the optimal choice is not reached by recurrently selecting a single option(arm), but by switching between two seemingly suboptimal choices. 

![Alt text](https://bitbucket.org/ViktorH/mab_project/raw/5db48c5747934bc6ad99bb73751e065c18952f73/Plots/MabVsMabNets.png?raw=true "Title")

# Simulations

In order to run a ready made simulation choose between the following:
JMab\plots\regrets
/binaryBandits or
/symbioticBandits

where a script with the range of either 8, 32 or 128 arm version of a simulation is available.

# A word of caution:

If you want to rerun a simulation to generate data, configure the asamp and plays constant to a small number such as 1000 or less.
Also comment the versions you do not want to run, as the combination of many different MAB simulation of large samples sizes and length can take a long time to finish.


# Requirements
Julia 0.5
Load the JMab module in memory.

Packages:
Gadfly
DataFrames
JLD
Distributions

# Results
Regret is measured by the reward from the optimal choice minus the agents choice.

![Alt text](https://bitbucket.org/ViktorH/mab_project/raw/67f815d7ddeff5d48d1c0dfc5b446384026f54e0/JMab/plots/print/8bSingles.png?raw=true "Title")

![Alt text](https://bitbucket.org/ViktorH/mab_project/raw/67f815d7ddeff5d48d1c0dfc5b446384026f54e0/JMab/plots/print/b128AllRegrets.png?raw=true "Title")

![Alt text](https://bitbucket.org/ViktorH/mab_project/raw/67f815d7ddeff5d48d1c0dfc5b446384026f54e0/JMab/plots/print/symb32AllRegrets.png?raw=true "Title")